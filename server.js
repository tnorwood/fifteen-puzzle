const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');
const os = require('os');
const networkInterfaces = os.networkInterfaces();
const app = express();

const compiler = webpack(webpackConfig);

app.use(express.static(__dirname + '/static'));

const server = app.listen(3000, '0.0.0.0', function() {
  const host = server.address().address;
  console.log('Cute Files is running at http://%s:3000', host);
  console.log('Network Interfaces:\n');

  for(var key in networkInterfaces){
    networkInterfaces[key].forEach(function(obj){
      if(obj.family === 'IPv4'){
        console.log('\t' + key + ' : ' + obj.address);
      }
    });
  }
  console.log('');
});
