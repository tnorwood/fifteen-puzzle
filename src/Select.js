import React, {Component} from 'react';
import './Select.css';
import SelectItem from './SelectItem';

class Select extends Component {
  renderItems(nom, bol, i) {
    return <SelectItem key={i} name={nom} chosen={bol} onClick={() => this.props.onClick(nom)} />;
  }

  render() {
    const chosen = this.props.chosen;
    let items = this.props.images;
    let its = [];

    for (let i = 0; i < items.length; i++){
      if (items[i] === chosen){
        its.push(this.renderItems(items[i], true, i));
      } else {
        its.push(this.renderItems(items[i], false, i));
      }

    }

    return (<div className="select">
              {its}
            </div>
           );
  }
}

export default Select;
