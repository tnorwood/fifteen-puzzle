import React, {Component} from 'react';
import './SelectItem.css';

class SelectItem extends Component {
  render() {
    const nom = this.props.name;
    const source = require('./Assets/' + nom + '/' + nom + '.png');


    let bor = this.props.chosen ? '3px solid blue' : '1px solid black';
    const borderstyle = {border: bor}

    return (<div className="selectItemContainer">
              <div className="imageContainer">
                <img className="bord" src={source} alt={nom} width="100%" style={borderstyle} onClick={this.props.onClick} />
              </div>
              <div className="captionContainer">
                {nom}
              </div>
            </div>
           );
  }
}

export default SelectItem;
