import React, {Component} from 'react';
import './Info.css';

class Info extends Component {
  render() {
    return (<div className="infoContainer">
              <div className="infoButtonContainer">
                <button onClick={this.props.onClick}>New Game</button>
              </div>
              <div className="info">
                <h3 className="infoHeader">
                  Welcome to Slider Puzzle Game!
                </h3>
                <p className="infoBody">
                  The objective is to reassemble the image by moving the
                  15 pieces around the puzzle. If you're playing the
                  'Classic' puzzle, you can put the numbers in order,
                  or you can try to make other&nbsp;
                  <a href="http://www.thinkfun.com/wp-content/uploads/2015/09/Fifte-4900-IN02.pdf" target="blank">
                  combinations</a>.
                </p>
                <p className="infoBody">
                  Use the arrow keys to move the pieces. The arrows signify
                  the direction a tile will move to fill the empty space.
                  The finished puzzle will have the empty space in the
                  lower right corner.
                </p>



              </div>
            </div>
          );
  }
}

export default Info;
