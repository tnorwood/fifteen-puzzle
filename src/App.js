import React, { Component } from 'react';
import icon from './icon.png';
import Board from './Board.js';
import Select from './Select';
import Info from './Info';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      squares: [16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16],
      images: ['Classic', 'Cat', 'Dog'],
      chosenImage: "Classic",
      currentImage: "Classic",
      gameStart: false,
      gameWon: false
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.checkGameWin = this.checkGameWin.bind(this);
  }

  shuffle(){ //Durstenfield Shuffle
    let inversions = 0;
    let squares;

    while (inversions % 2 === 0) {
      squares = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
      for (let i = squares.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [ squares[i], squares[j] ] = [squares[j], squares[i] ];
      }

      let b = squares.indexOf(16)
      if (b !== 0){
        [ squares[b], squares[0] ] = [squares[0], squares[b] ];
      }

      inversions = this.countInversions(squares);
    }

    return squares;
  }

  countInversions(squares) {
    let inversions = 0;
    for (let i = 1; i < squares.length; i++) {
      let count = 0;
      for (let j = i; j < squares.length; j++) {
        if (squares[j] < squares[i]) {
          count++;
        }
      }
      inversions += count;
    }
    return inversions;
  }

  checkGameWin(squares) {
    let check = true;
    for (let i = 0; i < squares.length - 1; i++){
      if (squares[i] >= squares[i + 1]){
        check = false;
      }
    }

    if (check){
      this.setState({squares: [16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16],
                     gameWon: true,
                     gameStart: false});
    }
  }

  convertTo1D(i, j) {
    return (i * 4) + j;
  }

  convertTo2D(n) {
    let i = Math.floor(n / 4);
    let j = n % 4;
    return [i, j];
  }

  swap (sqs, n, m){
    let squares = sqs.slice()
    squares[n] = squares[m];
    squares[m] = 16;
    return squares;
  }

  handleButtonClick() {
    this.setState({squares: this.shuffle(),
                   currentImage: this.state.chosenImage,
                   gameStart: true,
                   gameWon: false
                  });
  }

  handleClick(nom) {
    this.setState({chosenImage: nom});
  }

  handleKeyDown(e){
    //let squares = this.state.sqs.slice();
    let squares = this.state.squares;
    let n = squares.indexOf(16);
    let i,j,m;
    [i, j] = this.convertTo2D(n);

    if (this.state.gameStart){
      switch(e.keyCode) {
        case 37: //left
          e.preventDefault();
          if (j !== 3) {
            m = this.convertTo1D(i, (j+1));
            let sqs = this.swap(squares, n, m);
            this.setState({squares:sqs});
          }
          break;
        case 39: //right
          e.preventDefault();
          if (j !== 0) {
            m = this.convertTo1D(i, (j-1));
            let sqs = this.swap(squares, n, m);
            this.setState({squares:sqs});
          }
          break;
        case 38: //up
          e.preventDefault();
          if (i !== 3) {
            m = this.convertTo1D((i+1), j);
            let sqs = this.swap(squares, n, m);
            this.setState({squares:sqs});
          }
          break;
        case 40: //down
          e.preventDefault();
          if (i !== 0) {
            m = this.convertTo1D((i-1), j);
            let sqs = this.swap(squares, n, m);
            this.setState({squares:sqs});
          }
          break;
        default:
          break;
      }
    }
    this.checkGameWin(this.state.squares);
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={icon} className="App-logo" alt="logo" />
          <h1>Slider Puzzle Game</h1>
        </div>
        <div className="App-sub left">
        <Select chosen={this.state.chosenImage}
                images={this.state.images}
                onClick={nom => this.handleClick(nom)}
                />
        </div>
        <div className="App-sub center">
        <Board squares={this.state.squares}
               start={this.state.gameStart}
               current={this.state.currentImage}
               won={this.state.gameWon}
               />
        </div>
        <div className="App-sub right">
          <Info onClick={this.handleButtonClick} />
        </div>

      </div>

    );
  }
}

export default App;
