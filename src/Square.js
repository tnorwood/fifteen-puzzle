import React from 'react';
import './Square.css';

const Square = (props) => {
  let c = props.image;
  let n = props.nvalue;
  let m = props.mvalue;
  let i = Math.floor(n / 4);
  let j = n % 4;

  const squarestyle = {
    top: (i * 125),
    left: (j * 125)
  };

  const source = require('./Assets/' + c + '/' + m + '.png');

  return (<div className="square" style={squarestyle}>
            <img className="squareimg" src={source} alt={m} />
          </div>
         );
}

export default Square;
