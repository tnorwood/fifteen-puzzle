import React, {Component} from 'react';
import Square from './Square';
import './Board.css';

class Board extends Component {
  rendersquare(n, m){
    return <Square key={n} nvalue={n} mvalue={m} image={this.props.current} />;
  }

  render(){
    let start = this.props.start;
    let won = this.props.won;
    let current = this.props.current;
    const source = require('./Assets/' + current + '/' + current + '.png');

    let sqs = [];
    for (let n = 0; n < this.props.squares.length; n++) {
      let m = this.props.squares[n];
      if (m === 16) { continue; }
      sqs.push(this.rendersquare(n, m));
    }

    let other;
    if (!start && !won) {
      other = (<div className="intro">
                 <p className='bold'>Pick an image and press 'New Game' to begin!</p>
               </div>
              );
    }
    if (won && !start) {
      other = <img src={source} width="500px" height="500px" alt={current} />;
    }

    return <div className="board">{sqs}{other}</div>;
  }
}

export default Board;
