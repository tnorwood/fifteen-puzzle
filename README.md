Fifteen-Puzzle
---

Fifteen-Puzzle is an in-browser version of the classic puzzle game.
This project is hosted on Gitlab Pages. You can play Fifteen-Puzzle
[here] (http://tnorwood.gitlab.io/fifteen-puzzle/)!

Installation
---

If you want to serve this project to your own server, clone this repo and run this command:

```
npm install
```

Then build the project with this command:

```
npm run compile
```


Usage
---

Start the server with this command:

```
npm start
```
